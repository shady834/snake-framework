<?php
require './libs/View.php';

class ErrorView extends View
{

	public function not_found( )
	{
		$this->render( 'errorNotFound' );
	}

}
?>