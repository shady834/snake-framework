<?php
require './libs/View.php';

class StartView extends View
{

	public function index( )
	{
		$start = $this->loadModel( 'start' );
		$this->render( 'startIndex' );
	}

}
?>