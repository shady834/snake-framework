<?php
abstract class Adapter
{

	public function loadView( $name, $path='./amv/views/' )
	{
		$path .= $name . '.php';
		$name .= 'View';

		if ( is_file( $path ) )
		{
			require $path;
			return new $name( );
		}
		else
		{
			exit( 'Widok ' . $name . ' nie istnieje!' );
		}
	}

	public function loadModel( $name, $path='./amv/models/' )
	{
		$path .= $name . '.php';
		$name .= 'Model';

		if ( is_file( $path ) )
		{
			require $path;
			return new $name( );
		}
		else
		{
			exit( 'Model ' . $name . ' nie istnieje!' );
		}
	}

}
?>