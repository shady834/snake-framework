<?php
abstract class View
{

	private $templates_path = './templates/';
	private $compile_path = './templates/compile/';

	public function render( $name )
	{
		$path = $this->compile_path . $name . '.php';

		if ( is_file( $path ) )
		{
			require $path;
		}
		else
		{
			$path_src = $this->templates_path . $name . '.stf';

			if ( is_file( $path_src ) )
			{
				$this->compile( $name, $path_src );
				require $path;
			}
			else
			{
				exit( 'Szablon ' . $name . ' nie istnieje!' );
			}
		}
	}

	public function compile( $name, $path )
	{
		$src = file( $path );
		$tpl = '';

		$lang = array( 
			'/{#(.*)#}/', 
			'/{\^(.*)}/',
			'/{\$(.*) = (.*)}/',
			'/\$_(.*)/', 
			'/\$([a-zA-Z].*)/', 
			'/{if (.*)}/', 
			'/{elseif (.*)}/', 
			'/{else}/', 
			'/{endif}/', 
			'/{while (.*)}/', 
			'/{endwhile}/', 
			'/{foreach (.*)}/', 
			'/{endforeach}/',
			'/{/', 
			'/}/',
			);
		$parser = array( 
			'', 
			'<?php echo $1; ?>',
			'<?php $$1 = $2; ?>',
			'$_$1',
			'$this->$1',
			'<?php if ($1): ?>', 
			'<?php elseif ($1): ?>', 
			'<?php else: ?>', 
			'<?php endif; ?>', 
			'<?php while ($1): ?>', 
			'<?php endwhile; ?>', 
			'<?php foreach ($1): ?>',
			'<?php endforeach; ?>',
			'<?php ', 
			' ?>'
			);

		foreach ( $src as $line )
		{
			if ( strstr( $line, 'include' ) ) {
				$include = preg_replace( '/{include \'(.*)\'}/', $this->templates_path . '$1', $line );
				$include = trim( $include );

				if ( is_file( $include ) )
				{
					$line = file_get_contents( $include );
				}
				else
				{
					exit( 'Szablon ' . $include . ' nie istnieje!' );
				}
			}
			$tpl .= preg_replace( $lang, $parser, $line );
		}

		# $tpl = preg_replace( '/\s+/', ' ', $tpl );
		file_put_contents( $this->compile_path . $name . '.php', $tpl );
	}

	public function assign( $var, $val )
	{
		$this->$var = $val;
	}

	public function loadModel( $name, $path='./amv/models/' )
	{
		$path .= $name . '.php';
		$name .= 'Model';

		if ( is_file( $path ) )
		{
			require $path;
			return new $name( );
		}
		else
		{
			exit( 'Model ' . $name . ' nie istnieje!' );
		}
	}

}
?>