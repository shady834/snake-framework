<?php
class Mailer
{

	private static $mailer;
	private static $attachment = false;

	public static function connect( $host, $user, $pass, $port )
	{
		require './libs/mailer/class.phpmailer.php';

		self::$mailer = new PHPMailer( );

		self::$mailer->PluginDir = './libs/mailer/';
		self::$mailer->CharSet = 'UTF-8';
		self::$mailer->SetLanguage( 'pl', './libs/mailer/' );

		self::$mailer->Host = $host;
		self::$mailer->Username = $user;
		self::$mailer->Password = $pass;

		self::$mailer->Mailer = 'smtp';
		self::$mailer->SMTPAuth = true;
		self::$mailer->Port = $port;
	}

	public static function msg( $from_email, $from_name, $subject, $body, $html=true, $altBody='' )
	{
		self::$mailer->From = $from_email;
		self::$mailer->FromName = $from_name;

		self::$mailer->IsHTML( $html );

		self::$mailer->Subject = $subject;
		self::$mailer->Body = $body;

		if ( empty( $altBody ) ) 
		{
			self::$mailer->AltBody = $body;
		}
		else
		{		
			self::$mailer->AltBody = $altBody;	
		}
	}

	public static function attach( $file, $name )
	{
		self::$attachment = true;
		self::$mailer->AddAttachment( $file, $name );
	}

	public static function attach64( $src, $name, $mime )
	{
		self::$attachment = true;
		self::$mailer->addStringAttachment( $src, $name, 'base64', $mime );
	}

	public static function send( $to_email, $to_name, $debugger=false )
	{
		try
		{
			self::$mailer->AddAddress( $to_email, $to_name );
			self::$mailer->Send( );
			self::clear( );
		}
		catch ( phpmailerException $e )
		{
			self::clear( );

			if ( $debugger )
			{
				exit ( 'Wystapil blad podczas wysylania email: ' . $e->errorMessage( ) );
			}
			else
			{
				exit ( 'Wystapil blad podczas wysylania email' );
			}
		}
		catch ( Exception $e )
		{
			self::clear( );

			if ( $debugger )
			{
				exit ( 'Wystapil nieoczekiwany blad podczas wysylania email: ' . $e->errorMessage( ) );
			}
			else
			{
				exit ( 'Wystapil nieoczekiwany blad podczas wysylania email' );
			}
		}
	}

	public static function clear( )
	{
		self::$mailer->ClearAddresses( );

		if ( isset( self::$attachment ) )
		{
			self::$attachment = false;
			self::$mailer->ClearAttachments( );
		}
	}

}
?>