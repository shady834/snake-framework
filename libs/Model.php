<?php
class DB
{

	public static $db;

	public function connect( )
	{
		self::$db = new mysqli( Config::$DB['server'], Config::$DB['user'], Config::$DB['pass'], Config::$DB['base'], Config::$DB['port'] );

		if ( self::$db->errno ) exit( 'Blad polaczenia z baza danych!' );

		self::$db->set_charset = Config::$DB['charset'];
	}

	public static function query( $q, $debugger=false )
	{
		if ( $debugger )
		{
			if ( $result = self::$db->query( $q ) )
			{
				return $result;
			}
			else
			{
				exit( '<h2>DB DEBUGGER:</h2>' . self::$db->error );
			}
		}
		else
		{
			return self::$db->query( $q );
		}
	}

	public static function num_rows( $result )
	{
		return $result->num_rows;
	}

	public static function last_insert_id( )
	{
		return self::$db->insert_id;
	}

	public static function qfetch( $result, $type='assoc' )
	{
		switch ( $type )
		{
			default:
			case 'assoc':
				return $result->fetch_assoc( );
				break;

			case 'row':
				return $result->fetch_row( );
				break;

			case 'object':
				return $result->fetch_object( );
				break;
		}
	}

	public static function fetch( $q, $debugger=false, $type='assoc' )
	{
		$result = self::query( $q, $debugger );

		switch ( $type )
		{
			default:
			case 'assoc':
				return $result->fetch_assoc( );
				break;

			case 'row':
				return $result->fetch_row( );
				break;

			case 'object':
				return $result->fetch_object( );
				break;
		}
	}

	public static function multi_qfetch( $result, $type='assoc' )
	{
		switch ( $type )
		{
			default:
			case 'assoc':
				$data = array( );
				while( $row = $result->fetch_assoc( ) )
				{
					$data[] = $row;
				}
				return $data;
				break;

			case 'row':
				$data = array( );
				while( $row = $result->fetch_row( ) )
				{
					$data[] = $row;
				}
				return $data;
				break;

			case 'object':
				$data = array( );
				while( $row = $result->fetch_object( ) )
				{
					$data[] = $row;
				}
				return $data;
				break;
		}
	}

	public static function multi_fetch( $q, $debugger=false, $type='assoc' )
	{
		$result = self::query( $q, $debugger );

		switch ( $type )
		{
			default:
			case 'assoc':
				$data = array( );
				while( $row = $result->fetch_assoc( ) )
				{
					$data[] = $row;
				}
				return $data;
				break;

			case 'row':
				$data = array( );
				while( $row = $result->fetch_row( ) )
				{
					$data[] = $row;
				}
				return $data;
				break;

			case 'object':
				$data = array( );
				while( $row = $result->fetch_object( ) )
				{
					$data[] = $row;
				}
				return $data;
				break;
		}
	}

	public static function queryClose( $query )
	{
		$query->close( );
	}

	public static function connectionClose( )
	{
		self::$db->close( );
	}

}

abstract class Model
{

	protected $player;

	public function filter( $data, $type = 's' )
	{
		switch ( $type )
		{
			default:
			case 's':
				return filter_var( trim( $data ), FILTER_SANITIZE_STRING );
				break;
			case 'n':
				return filter_var( $data, FILTER_SANITIZE_NUMBER_INT );
				break;
			case 'f':
				return filter_var( $data, FILTER_SANITIZE_NUMBER_FLOAT );
				break;
		}
	}

	public function crypto( $data, $salt )
	{
		return hash( 'sha256', $salt . trim( $data ) . Config::$PASSWORD_SALT );
	}

	public function setPlayer( $q )
	{
		$player = DB::fetch( $q );

		if ( empty( $player ) )
		{
			Router::setTrace( 'task-member/action-logout' );
		}
		elseif ( Session::getSess( 'time' ) < time( ) )
		{
			Router::setTrace( 'task-member/action-logout' );
		}
		elseif ( Session::getSess( 'prev' ) > 14 )
		{
			Session::Regenerate( );
			Session::setSess( 'time', time( ) + 1200 );
			Session::setSess( 'prev', 1 );
		}
		else
		{
			Session::setSess( 'time', time( ) + 1200 );
			Session::plusOne( 'prev' );
		}

		$this->player = $player;
	}

	public function uniqKey( $length = 10 )
	{
		$string = '';
		$chars 	= '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$i     	= 0;

		while ( $i < $length )
		{
			$string .= substr( $chars, mt_rand( 0, strlen( $chars ) - 1 ), 1 );
			$i++;
		}

		return $string;
	}

	public function trueIP( )
	{
		if ( isset( $_SERVER['HTTP_CLIENT_IP'] ) ) return $_SERVER['HTTP_CLIENT_IP'];
		elseif ( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) return $_SERVER['HTTP_X_FORWARDED_FOR'];
		else return $_SERVER['REMOTE_ADDR'];
	}

	public function loadModel( $name, $path='./amv/models/' )
	{
		$path .= $name . '.php';
		$name .= 'Model';

		if ( is_file( $path ) )
		{
			require $path;
			return new $name( );
		}
		else
		{
			exit( 'Model ' . $name . ' nie istnieje!' );
		}
	}

}
?>