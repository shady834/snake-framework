<?php
/**
 * @package: Snake Framework
 * @version: 0.0.1.0 <beta>
 */

/* załaduj plik konfiguracyjny */
require_once './config.php';

class Router
{

	public static function getTrace( )
	{
		$path  = array_filter( explode( '/', str_replace( Config::$ROOT, '', $_SERVER['REQUEST_URI'] ) ) );
		$trace = array( );

		foreach ( $path as $track )
		{
			$track = explode( '-', $track );

			if ( empty( $track[1] ) )
			{
				$trace[$track[0]] = '';
			}
			else
			{
				$trace[$track[0]] = $track[1];
			}
		}

		return $trace;
	}

	public static function setTrace( $trace )
	{
		header( 'Location: ' . Config::$ROOT . $trace );
		exit;
	}

}

class Session
{

	public static function Init( )
	{
		if ( session_status() == PHP_SESSION_NONE )
		{
			session_start( );
		}
		else
		{
			exit( 'Sesja zostala juz wystartowana!' );
		}
	}

	public static function Regenerate( )
	{
		session_regenerate_id( ) ;
	}

	public static function setSess( $var, $val )
	{
		$_SESSION[$var] = $val;
	}

	public static function Exists( $var )
	{
		if ( isset( $_SESSION[$var] ) )
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public static function getSess( $var )
	{
		if ( isset( $_SESSION[$var] ) )
		{
			return $_SESSION[$var];
		}
		else
		{
			exit( 'Sesja ' . $var . ' nie istnieje!' );
		}
	}

	public static function plusOne( $var )
	{
		if ( isset( $_SESSION[$var] ) )
		{
			$_SESSION[$var]++;
		}
		else
		{
			exit( 'Sesja ' . $var . ' nie istnieje!' );
		}
	}

	public static function minusOne( $var )
	{
		if ( isset( $_SESSION[$var] ) )
		{
			$_SESSION[$var]--;
		}
		else
		{
			exit( 'Sesja ' . $var . ' nie istnieje!' );
		}
	}

	public static function Destroy( )
	{
		session_unset( );
		session_destroy( );
	}

}

class Init
{

	private $get;
	private $adapter;

	public function __construct( )
	{
		$this->get = Router::getTrace( );

		empty( $this->get['task'] ) ? $this->defaultAdapter( ) : $this->loadAdapter( );
	}

	public function defaultAdapter( )
	{
		$this->get['task'] = 'start';
		$this->get['action'] = 'index';

		require './amv/adapters/start.php';

		$this->adapter = new Start( );
		$this->adapter->Index( );
	}

	public function loadAdapter( )
	{
		$this->get['task'] = filter_var( $this->get['task'], FILTER_SANITIZE_STRING );

		$path = './amv/adapters/' . $this->get['task'] . '.php';

		if ( is_file( $path ) )
		{
			if( empty( $this->get['action'] ) )
			{
				Router::setTrace( 'task-error/action-not_found' );
			}
			else
			{
				require $path;
				$this->adapter = new $this->get['task']( );
				$this->loadAction( );
			}
		}
		else
		{
			Router::setTrace( 'task-error/action-not_found' );
		}
	}

	public function loadAction( )
	{
		$actionName = filter_var( $this->get['action'], FILTER_SANITIZE_STRING );

		if ( method_exists( $this->adapter, $actionName ) )
		{
			$this->adapter->$actionName( );
		}
		else
		{
			Router::setTrace( 'task-error/action-not_found' );
		}
	}

}

new Init( );
?>