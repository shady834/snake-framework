<?php
class Config
{

	/* katalog nadrzędny zainstalowanego skryptu */
	public static $ROOT = '/snake/';

	/* 20 znakowy unikatowy klucz, służący jako stała sól do haseł */
	public static $PASSWORD_SALT = 'B76Bv3y8TYv4h78fg7Kl';

	/* dane do nawiązania połączenia z bazą danych */
	public static $DB = array( 
		'server' => 'localhost',
		'user' => 'root',
		'pass' => '',
		'base' => 'micro',
		'port' => 3360,
		'charset' => 'utf8mb4',
	);

	public function __construct( )
	{
		/* domyślna strefa czasowa */
		date_default_timezone_set( 'Europe/Warsaw' );

		/* poziom raportowania błędów */
		/* wersja developerska: -1; wersja publiczna: 0 */
		error_reporting( -1 );
	}

}
?>